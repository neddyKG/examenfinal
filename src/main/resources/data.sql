INSERT INTO role(name)
VALUES("ADMIN");

INSERT INTO role(name)
VALUES("PROPIETARIO");

INSERT INTO role(name)
VALUES("CLIENTE");

INSERT INTO country(id, name)
VALUES(1, "Bolivia");

INSERT INTO country(id, name)
VALUES(2, "EEUU");

INSERT INTO city(id, name, country_id)
VALUES(1, "Cbba", 1);

INSERT INTO city(id, name, country_id)
VALUES(2, "Virginia", 2);

INSERT INTO restaurant (id, direction, name, phone, latitude, longitude, city_id)
VALUES(1, "America","Ellis","4455555", -17.371, -66.143, 1);

INSERT INTO product(id, amount, name, restaurant_id)
VALUES(1, 49, "Alfredo", 1);

INSERT INTO product(id, amount, name, restaurant_id)
VALUES(2, 70, "Margarita", 1);

INSERT INTO product(id, amount, name, restaurant_id)
VALUES(3, 50, "Carnivora", 1);

INSERT INTO restaurant (id, direction, name, phone, latitude, longitude, city_id)
VALUES(2, "Circunvalacion","Malcriadas","4447777", -17.368, -66.145, 2);

INSERT INTO product(id, amount, name, restaurant_id)
VALUES(4, 68, "Jamon y queso", 2);

INSERT INTO product(id, amount, name, restaurant_id)
VALUES(5, 70, "Hawaiana", 2);

INSERT INTO product(id, amount, name, restaurant_id)
VALUES(6, 95, "BBQ", 2);

--CLIENTE

INSERT INTO user (id, email, username, lastname, address, cellphone, phone, password, password_Confirm, restaurant_id, product_id, city_id)
VALUES (1, "c@gmail.com","Pepito", "Mamani", "Pacata Alta", 4481520, 70764525, "$2a$10$Wj.C5mIy1.xBZuvpXvZGP./jpMjPC5JsJ7Z1UXPpkO01Tt4hM.dv6", "1", 1, 1, 1);

INSERT INTO user_role(user_id, role_id)
VALUES(1,3);

INSERT INTO direction(address, latitude, longitude, state, user_id)
VALUES("Circunvalacion",-17.367, -66.147, 1, 1);

INSERT INTO direction(address, latitude, longitude, state, user_id)
VALUES("Borda",-17.367, -66.148, 1, 1);


INSERT INTO user (id, email, username, lastname, address, cellphone, phone, password, password_Confirm, restaurant_id, product_id, city_id)
VALUES (2, "c2@gmail.com","Juanita", "Choque", "Pacata Baja", 42156467, 7070015,"$2a$10$Wj.C5mIy1.xBZuvpXvZGP./jpMjPC5JsJ7Z1UXPpkO01Tt4hM.dv6", "1", 2, 4, 2);

INSERT INTO user_role(user_id, role_id)
VALUES(2,3);

INSERT INTO direction(address, latitude, longitude, state, user_id)
VALUES("Girasoles",-17.367, -66.149, 1, 2);

INSERT INTO direction(address, latitude, longitude, state, user_id)
VALUES("Uyuni",-17.378, -66.149, 1, 2);
--

--PROPIETARIOS
INSERT INTO user (id, email, username, lastname, address, cellphone, phone, password, password_Confirm, restaurant_id)
VALUES (3, "p@gmail.com","Pablo", "Gonsalez", "Pacata Alta", 4481520, 70764525, "$2a$10$Wj.C5mIy1.xBZuvpXvZGP./jpMjPC5JsJ7Z1UXPpkO01Tt4hM.dv6", "1", 1);

INSERT INTO user_role(user_id, role_id)
VALUES(3,2);

INSERT INTO user (id, email, username, lastname, address, cellphone, phone, password, password_Confirm, restaurant_id)
VALUES (4, "p2@gmail.com","Flor", "Villaroel", "Pacata Baja", 42156467, 7070015,"$2a$10$Wj.C5mIy1.xBZuvpXvZGP./jpMjPC5JsJ7Z1UXPpkO01Tt4hM.dv6", "1", 2);

INSERT INTO user_role(user_id, role_id)
VALUES(4,2);
--

--ADMINISTRADOR
INSERT INTO user (id, email, username, lastname, address, cellphone, phone, password, password_Confirm)
VALUES (5, "a@gmail.com","Diego", "Salgueiro", "hipermaxi", 4481520, 70764525, "$2a$10$Wj.C5mIy1.xBZuvpXvZGP./jpMjPC5JsJ7Z1UXPpkO01Tt4hM.dv6", "1");

INSERT INTO user_role(user_id, role_id)
VALUES(5,1);

--

