package com.ucbcba.proyecto.services;

import com.ucbcba.proyecto.entities.Country;

public interface CountryService {

    Iterable<Country> listAllCountries();

    Country getCountryById(Integer id);

    Country saveCountry(Country country);

    void deleteCountry(Integer id);

}

