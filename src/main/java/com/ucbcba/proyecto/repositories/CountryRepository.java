package com.ucbcba.proyecto.repositories;

import com.ucbcba.proyecto.entities.Country;
import com.ucbcba.proyecto.entities.Restaurant;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.transaction.Transactional;

@Transactional
public interface CountryRepository extends JpaRepository<Country, Integer> {

}
