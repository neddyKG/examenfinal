package com.ucbcba.proyecto.controllers;

import com.ucbcba.proyecto.entities.Restaurant;
import com.ucbcba.proyecto.services.CityService;
import com.ucbcba.proyecto.services.RestaurantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.Iterator;

@Controller
public class RestaurantController {

    private RestaurantService restaurantService;
    private CityService cityService;

    @Autowired
    public void setCityService(CityService cityService) {
        this.cityService = cityService;
    }

    @Autowired
    public void setRestaurantService(RestaurantService restaurantService) {
        this.restaurantService = restaurantService;
    }

    @RequestMapping(value = "/admin/restaurant/new", method = RequestMethod.GET)
    public String newRestaurant(Model model) {

        model.addAttribute("restaurant", new Restaurant());
        model.addAttribute("cities", cityService.listAllCitys());

        return "restaurantForm";
    }
    @RequestMapping(value = "/admin/restaurant", method = RequestMethod.POST)
    public String save(@Valid Restaurant restaurant, BindingResult bindingResult, Model model) {
        if(bindingResult.hasErrors()){
            model.addAttribute("cities", cityService.listAllCitys());
            return "restaurantForm";
        }

        restaurantService.saveRestaurant(restaurant);

        return "redirect:/admin/restaurants";
    }

    /* List all restaurants.
     *
     * @param model
     * @return
     */

    @RequestMapping(value = "/admin/restaurant/{id}", method = RequestMethod.GET)
    public String showRestaurant(@PathVariable Integer id, Model model) {
        Restaurant restaurant = restaurantService.getRestaurantById(id);
        model.addAttribute("restaurant", restaurant);
        return "restaurant";
    }

    @RequestMapping(value = "/admin/restaurant/editar/{id}", method = RequestMethod.GET)
    public String editRestaurant(@PathVariable Integer id, Model model) {
        model.addAttribute("restaurant", restaurantService.getRestaurantById(id));
        model.addAttribute("cities", cityService.listAllCitys());
        return "restaurantForm";
    }

    @RequestMapping(value = "/admin/restaurant/eliminar/{id}", method = RequestMethod.GET)
    public String deleteRestaurant(@PathVariable Integer id, Model model) {
        Restaurant eliminar=restaurantService.getRestaurantById(id);
        restaurantService.deleteRestaurant(id);
        return "redirect:/admin/restaurants";
    }

    @RequestMapping(value = "/admin/restaurants", method = RequestMethod.GET)
    public String list(Model model) {
        model.addAttribute("restaurants", restaurantService.listAllRestaurants());
        return "restaurants";
    }


}
