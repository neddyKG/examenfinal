package com.ucbcba.proyecto.controllers;



import com.ucbcba.proyecto.entities.*;
import com.ucbcba.proyecto.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Set;

@Controller
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private RestaurantService restaurantService;

    @Autowired
    private ProductService productService;

    @Autowired
    private DirectionService directionService;


    @Autowired
    private OrderService orderService;

    @Autowired
    private CityService cityService;

    @Autowired
    private CountryService countryService;

    //@Autowired
    //private UserValidator userValidator;

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registrationInit(Model model) {
        model.addAttribute("user", new User());
        model.addAttribute("cities",cityService.listAllCitys());
        model.addAttribute("countrys",countryService.listAllCountries());
        return "registration";//implementar html registration Carlos
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registration(@Valid User user, BindingResult bindingResult, Model model) {
        ///userValidator.validate(userForm, bindingResult);
        if (bindingResult.hasErrors()) {
            model.addAttribute(user);
            model.addAttribute("cities",cityService.listAllCitys());
            model.addAttribute("countrys",countryService.listAllCountries());
            return "registration";
        }

        userService.save(user);
        securityService.autologin(user.getEmail(), user.getPasswordConfirm());
        Set<String> roles = AuthorityUtils.authorityListToSet(SecurityContextHolder.getContext().getAuthentication().getAuthorities());
        if (roles.contains("ADMIN")) {
            return "redirect:/admin/home";
        }
        if (roles.contains("CLIENTE")) {
            return "redirect:/user/country/city";
        }
        return "redirect:/";
    }




    //USER COUNTRY CITY
    @RequestMapping(value = "/user/country/city", method = RequestMethod.GET)
    public String CityUser(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByEmail(auth.getName());

        model.addAttribute("user",user);
        model.addAttribute("country",user.getCountry());

        return "cityUser";
    }


    @RequestMapping(value = "/user/country/city", method = RequestMethod.POST)
    public String CityUserSave(@Valid User user, BindingResult bindingResult, Model model) {

        if(bindingResult.hasErrors()) {
            //System.out.println(bindingResult.getAllErrors().toString());
            return "cityUser";
        }
        userService.saveUserEdited(user);
        return "redirect:/user/home";
    }


    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String login(Model model, String error, String logout) {
        Set<String> roles = AuthorityUtils.authorityListToSet(SecurityContextHolder.getContext().getAuthentication().getAuthorities());
        if (roles.contains("ADMIN")) {
            return "redirect:/admin/home";
        }
        if (roles.contains("CLIENTE")) {
            return "redirect:/user/home";
        }
        if (roles.contains("PROPIETARIO")) {
            return "redirect:/propietario/home";
        }
        return "redirect:/";
    }

    @RequestMapping(value = "/admin/home", method = RequestMethod.GET)
    public String homeA(Model model){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByEmail(auth.getName());
        model.addAttribute("nameA",user.getUsername() + " " + user.getLastname());
        return "principalA";
    }

    @RequestMapping(value = "/propietario/home", method = RequestMethod.GET)
    public String homeP(Model model){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByEmail(auth.getName());
        model.addAttribute("nameP",user.getUsername() + " " + user.getLastname());
        return "principalP";
    }

    ////////////////USER ROUTES
    @RequestMapping(value = "/user/home", method = RequestMethod.GET)
    public String homeU(Model model){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByEmail(auth.getName());
        model.addAttribute("nameA",user.getUsername() + " " + user.getLastname());
        return "principalU";
    }
    @RequestMapping(value = "/user/opcion")
    public String opcion()
    {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByEmail(auth.getName());
        if(user.getRestaurant()==null){
            return "redirect:/user/restaurante";
        } else {
            if(user.getProduct()==null){
                return "redirect:/user/pedido";
            } else {
                return "redirect:/user/defecto";
            }
        }
    }

    @RequestMapping(value = "/user/restaurante", method = RequestMethod.GET)
    public String Restaurante(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByEmail(auth.getName());
        model.addAttribute("restaurants",restaurantService.listAllRestaurants());
        model.addAttribute("user",user);


        return "opcionRestaurante";
    }

    @RequestMapping(value = "/user/restaurante", method = RequestMethod.POST)
    public String restaurantSave(@Valid User user, BindingResult bindingResult, Model model) {

        if(bindingResult.hasErrors()) {
            //System.out.println(bindingResult.getAllErrors().toString());
            return "opcionRestaurante";
        }

        userService.saveUserEdited(user);
        return "redirect:/user/pedido";
    }


    //PRODUCTO PEDIDO
    @RequestMapping(value = "/user/pedido", method = RequestMethod.GET)
    public String Pedido(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByEmail(auth.getName());

        model.addAttribute("user",user);
        model.addAttribute("restaurant",user.getRestaurant());

        return "productRestaurant";
    }


    @RequestMapping(value = "/user/pedido", method = RequestMethod.POST)
    public String pedidoSave(@Valid User user, BindingResult bindingResult, Model model) {

        if(bindingResult.hasErrors()) {
            //System.out.println(bindingResult.getAllErrors().toString());
            return "productRestaurant";
        }
        userService.saveUserEdited(user);
        return "redirect:/user/defecto";
    }

    //@RequestMapping(value = {"/", "/welcome"}, method = RequestMethod.GET)
    @RequestMapping(value = {"/welcome"}, method = RequestMethod.GET)
    public String welcome(Model model) {
        return "welcome";
    }
    @RequestMapping(value = {"/admin/"}, method = RequestMethod.GET)
    public String admin(Model model) {
        return "welcome";
    }
    @RequestMapping(value = {"/bienvenidos"}, method = RequestMethod.GET)
    public String welcome2(Model model) {
        Set<String> roles = AuthorityUtils.authorityListToSet(SecurityContextHolder.getContext().getAuthentication().getAuthorities());
        if (roles.contains("ADMIN")) {
            return "redirect:/admin/home";
        }
        if (roles.contains("CLIENTE")) {
            return "redirect:/user/home";
        }
        if (roles.contains("PROPIETARIO")) {
            return "redirect:/propietario/home";
        }
        return "redirect:/";
    }

    @RequestMapping(value = "/user/direction", method = RequestMethod.GET)
    public String direction(Model model){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByEmail(auth.getName());
        model.addAttribute("user",user);
        model.addAttribute("direction", new Direction(user));
        return "directionForm";
    }


    @RequestMapping(value = "/user/directions", method = RequestMethod.GET)
    public String directions(Model model){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByEmail(auth.getName());
        model.addAttribute("user",user);
        return "drections";
    }

    @RequestMapping(value = "/user/direction", method = RequestMethod.POST)
    public String saveDirection(@Valid Direction direction, BindingResult bindingResult, Model model){
        if(bindingResult.hasErrors()){
            User user = direction.getUser();
            model.addAttribute("user", user);
            return "directionForm";
        }
        directionService.saveDirection(direction);
        return "redirect:/user/directions";
    }

    @RequestMapping(value = "/user/propietario/pedido", method = RequestMethod.POST)
    public String saveOrder(@Valid Order order, BindingResult bindingResult, Model model){
        if(bindingResult.hasErrors()){
            User user = order.getUser();
            model.addAttribute("user", user);
            return "order";
        }
        orderService.saveOrder(order);
        return "redirect:/propietario/pedidos";
    }

    @RequestMapping(value = "user/directions/eliminar/{id}", method = RequestMethod.GET)
    public String deleteDirection(@PathVariable Integer id,Model model){
        Direction direction=directionService.getDirectionById(id);
        direction.setState(0);
        directionService.saveDirection(direction);
        return "redirect:/user/directions";
    }
    @RequestMapping(value = "user/directions/editar/{id}", method = RequestMethod.GET)
    public String editDirection(@PathVariable Integer id, Model model) {
        model.addAttribute("direction", directionService.getDirectionById(id));
        return "directionForm";
    }

    @RequestMapping(value = "user/propietario/pedido/editar/{id}", method = RequestMethod.GET)
    public String editOrder(@PathVariable Integer id, Model model) {
        model.addAttribute("order", orderService.getOrderById(id));
        return "orderForm";
    }
    //MIS PEDIDOS
    @RequestMapping(value = "/user/orders", method = RequestMethod.GET)
    public String userPedidos(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByEmail(auth.getName());

        model.addAttribute("user", user);
        model.addAttribute("orders", user.getOrders());
        return "userOrders";
    }

    //mensaje de error si no es porpietario de ningun restaurant con exception handling
    @ResponseStatus(value= HttpStatus.NOT_FOUND, reason="No es propietario de ningun restaurant")  // 404
    public class OrderNotFoundException extends RuntimeException {

    }
    //pedidos como propietario
    @RequestMapping(value = "/propietario/pedidos", method = RequestMethod.GET)
    public String userPedidosP(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByEmail(auth.getName());
        if(user.getRestaurant()==null)
        {
            //throw new OrderNotFoundException();
           return "/principalP";
        }
        model.addAttribute("user", user);
        model.addAttribute("pedidos", orderService.findAllOrder());

        return "listaPedidos";
    }

    @RequestMapping(value = "/user/order/{id}", method = RequestMethod.GET)
    public String showRestaurant(@PathVariable Integer id, Model model) {
        Order order = orderService.getOrderById(id);
        model.addAttribute("order", order);

        /////////latitud y longitud del usuario
        model.addAttribute("lat",order.getDirection().getLatitude());
        model.addAttribute("lon",order.getDirection().getLongitude());

        /////////latitud y longitud del restaurante
        model.addAttribute("lat1",order.getProduct().getRestaurant().getLatitude());
        model.addAttribute("lon1",order.getProduct().getRestaurant().getLongitude());

        return "orderInfo";
    }


//EDITAR INFO DE USUARIO


    @RequestMapping(value = "/user/edit", method = RequestMethod.GET)
    public String userEdit( Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByEmail(auth.getName());

        model.addAttribute("cities",cityService.listAllCitys());
        model.addAttribute("countrys",countryService.listAllCountries());
        model.addAttribute("user", userService.getUserById(user.getId()));
        return "editUser";
    }

    @RequestMapping(value = "/user/save/edit", method = RequestMethod.POST)
    public String saveEditUser(@Valid User user, BindingResult bindingResult, Model model) {
        ///userValidator.validate(userForm, bindingResult);
        if (bindingResult.hasErrors()) {
            model.addAttribute(user);
            model.addAttribute("cities",cityService.listAllCitys());
            model.addAttribute("countrys",countryService.listAllCountries());
            return "editUser";
        }

        userService.saveUserEdited(user);
            return "redirect:/user/home";

    }

    @RequestMapping(value = "/user/opcion/edit")
    public String opcionEdit()
    {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByEmail(auth.getName());

        return "redirect:/user/restaurante";

    }


}
