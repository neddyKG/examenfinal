package com.ucbcba.proyecto.controllers;

import com.ucbcba.proyecto.entities.Order;
import com.ucbcba.proyecto.entities.User;
import com.ucbcba.proyecto.services.OrderService;
import com.ucbcba.proyecto.services.ProductService;
import com.ucbcba.proyecto.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
public class OrderController {

    private ProductService productService;

    @Autowired
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    private OrderService orderService;

    @Autowired
    public void setOrderService(OrderService orderService) {
        this.orderService = orderService;
    }

    @RequestMapping(value = "/user/defecto", method = RequestMethod.GET)
    public String orderUser(Model model){
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = userService.findUserByEmail(auth.getName());
        model.addAttribute("nameA",user.getUsername() + " " + user.getLastname());
        model.addAttribute("user",user);
        model.addAttribute("product", user.getProduct());
        Order order=new Order(user,user.getProduct());
        order.setPrice(order.getQuantity()*order.getProduct().getAmount());

        /////////latitud y longitud del restaurante
        model.addAttribute("lat1",user.getProduct().getRestaurant().getLatitude());
        model.addAttribute("lon1",user.getProduct().getRestaurant().getLongitude());


        model.addAttribute("orderU",order);

        return "buyProduct";
    }
    @RequestMapping(value = "/user/defecto", method = RequestMethod.POST)
    public String saveOrderUser(@Valid Order order, BindingResult bindingResult, Model model){
        if(bindingResult.hasErrors()){
            Authentication auth = SecurityContextHolder.getContext().getAuthentication();
            User user = userService.findUserByEmail(auth.getName());
            model.addAttribute("nameA",user.getUsername() + " " + user.getLastname());
            model.addAttribute("user",user);
            model.addAttribute("product", user.getProduct());
            Order order1=new Order(user,user.getProduct());
            order.setPrice(order1.getQuantity()*order1.getProduct().getAmount());

            return "buyProduct";
        }
        orderService.saveOrder(order);
        return "redirect:/user/home";
    }

}
