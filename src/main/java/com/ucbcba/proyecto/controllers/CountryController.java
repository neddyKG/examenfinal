package com.ucbcba.proyecto.controllers;

import com.ucbcba.proyecto.entities.Country;
import com.ucbcba.proyecto.services.CityService;
import com.ucbcba.proyecto.services.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Controller
public class CountryController {
    private CountryService countryService;
    private CityService cityService;

    @Autowired
    public void setCityService(CityService cityService) {
        this.cityService = cityService;
    }

    @Autowired
    public void setCountryService(CountryService countryService) {
        this.countryService = countryService;
    }

    @RequestMapping(value = "/admin/country/new", method = RequestMethod.GET)
    public String newCountry(Model model) {

        model.addAttribute("country", new Country());
        model.addAttribute("cities", cityService.listAllCitys());

        return "countryForm";
    }
    @RequestMapping(value = "/admin/country", method = RequestMethod.POST)
    public String save(@Valid Country country, BindingResult bindingResult, Model model) {
        if(bindingResult.hasErrors()){
            model.addAttribute("cities", cityService.listAllCitys());
            return "countryForm";
        }

        countryService.saveCountry(country);

        return "redirect:/admin/countrys";
    }

    /* List all countrys.
     *
     * @param model
     * @return
     */

    @RequestMapping(value = "/admin/country/{id}", method = RequestMethod.GET)
    public String showCountry(@PathVariable Integer id, Model model) {
        Country country = countryService.getCountryById(id);
        model.addAttribute("country", country);
        return "country";
    }

    @RequestMapping(value = "/admin/country/editar/{id}", method = RequestMethod.GET)
    public String editCountry(@PathVariable Integer id, Model model) {
        model.addAttribute("country", countryService.getCountryById(id));
        model.addAttribute("cities", cityService.listAllCitys());
        return "countryForm";
    }

    @RequestMapping(value = "/admin/country/eliminar/{id}", method = RequestMethod.GET)
    public String deleteCountry(@PathVariable Integer id, Model model) {
        Country eliminar=countryService.getCountryById(id);
        countryService.deleteCountry(id);
        return "redirect:/admin/countrys";
    }

    @RequestMapping(value = "/admin/countrys", method = RequestMethod.GET)
    public String list(Model model) {
        model.addAttribute("countrys", countryService.listAllCountries());
        return "countrys";
    }


}

